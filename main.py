#!/usr/bin/env python
import os
import subprocess

const_url = "http://172.19.30.121:8080/ui/mobile/jsp/"
dest_path = "/home/vv/media/screens/"

try:
	sourceFile1 = open("data/res", "rb")
except:
	print "Cannot find file with resolutions!\nExit..."
	exit(1)

try:
	sourceFile2 = open("data/uas", "rb")
except:
	print "Cannot find file with user agents!\nExit..."
	exit(1)

try:
	sourceFile3 = open("data/pages", "rb")
except:
	print "Cannot find file with pages!\nExit..."
	exit(1)

url = ""
page = ""
full_url = ""
filename = ""
userAgent = ""

while 1:
	url = sourceFile3.readline()[:-1]

	if not url:
		break

	while 1:
		userAgent = sourceFile2.readline()[:-1]

		if not userAgent:
			break

		while 1:
			resolution_str = sourceFile1.readline()[:-1]

			if not resolution_str:
				break

			full_url = const_url + url
			shortFilename = userAgent.replace("/", "_")
			page = url.split("/")
			page = page[len(page) - 1]
			filename = dest_path + shortFilename + "_" + resolution_str + "_" + page + ".png"
			arguments = ["python", "screen.py", userAgent, resolution_str, full_url, filename]
			subprocess.call(arguments);

		sourceFile1.seek(0)

	sourceFile2.seek(0)

print "the end"
