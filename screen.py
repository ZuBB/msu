#!/usr/bin/env python
import sys
import signal

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import QWebPage

class WebPage(QWebPage):
	def __init__(self, user_agent, confirm=True):
		QWebPage.__init__(self)
		self.user_agent = user_agent
		self.confirm = confirm

	def userAgentForUrl(self, url):
		return self.user_agent


def onLoadFinished(result):
	if not result:
		print "Request failed"
		sys.exit(1)

	# Paint this frame into an image
	image = QImage(webpage.viewportSize(), QImage.Format_ARGB32)
	painter = QPainter(image)
	webpage.mainFrame().render(painter)
	painter.end()
	# filename
	#print "saving " + qtargs[4]
	image.save(qtargs[4])
	print "Done"
	sys.exit(0)

qtargs = sys.argv[:]
qtargs.append("-display")
qtargs.append(":0")

app = QApplication(qtargs,True)
signal.signal(signal.SIGINT, signal.SIG_DFL)

# userAgent
webpage = WebPage(qtargs[1])
webpage.mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)
webpage.mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)

# resolution
dims = qtargs[2].split("x")
viewportSize = QSize(int(dims[0]), int(dims[1]))
webpage.setViewportSize(viewportSize)

# page
webpage.connect(webpage, SIGNAL("loadFinished(bool)"), onLoadFinished)
webpage.mainFrame().load(QUrl(qtargs[3]))

sys.exit(app.exec_())
